# frozen_string_literal: true

require 'toy_robot/robot'
require 'toy_robot/version'

# Provides interface for interacting with toy robot
module ToyRobot
  # must have a space after PLACE
  # allow extra whitespace between args
  COMMANDS = Regexp.union(
    [
      /\s*(MOVE)\s*/,
      /\s*(LEFT)\s*/,
      /\s*(RIGHT)\s*/,
      /\s*(REPORT)\s*/,
      /\s*(PLACE)\s+\s*([0-9])\s*,\s*([0-9])\s*,\s*(NORTH|SOUTH|EAST|WEST)\s*/
    ]
  ).freeze

  def self.parse_command(command_string)
    match = command_string.match(COMMANDS)
    return unless match

    method = find_method(match.captures[0..4])
    args = find_args(match.captures[5..-1])

    [method, *args]
  end

  def self.run(input, output)
    @input = input
    @output = output
    @interactive = input == $stdin
    @robot = ToyRobot::Robot.new

    process_input
  end

  def self.process_input
    @input.each_line do |line|
      line = line.strip
      print_help if ['?', 'help'].include?(line)
      break if line == 'exit'

      command, *args = parse_command(line)
      perform(command, args) if command

      print '<robot> ' if @interactive
    end
  end

  private_class_method def self.perform(command, args)
    robot_output = @robot.send(command, *args)

    # only `:report` returns something
    return unless command == :report && robot_output
    @output.print "Output: #{robot_output.join(', ')}\n"
  end

  private_class_method def self.find_method(captures)
    captures.join.downcase.to_sym
  end

  private_class_method def self.find_args(captures)
    pos_x, pos_y, direction = captures
    return unless pos_x

    [pos_x.to_i, pos_y.to_i, direction.downcase.to_sym]
  end

  private_class_method def self.print_help
    puts HELP_TEXT

    print '<robot> '
  end

  HELP_TEXT = <<~HELP_TEXT
    ? | help -- this help

    exit
      Exit the simulator.

    PLACE X,Y,DIRECTION
      Place the robot at (X, Y) facing DIRECTION.
      DIRECTION can be NORTH, EAST, SOUTH, or WEST.

    MOVE
      Move the robot one unit in the direction it is facing.

    LEFT
      Turn the robot 90° anti-clockwise.

    RIGHT
      Turn the robot 90° clockwise.

    REPORT
      Report the position and direction of the robot.

  HELP_TEXT

  private_constant :HELP_TEXT
end
