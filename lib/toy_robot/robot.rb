# frozen_string_literal: true

# Processes parsed commands
module ToyRobot
  class InvalidPositionError; end

  # each direction points to its left and right directions (like a linked list)
  # delta_x and delta_y are a vector representation of moving 1 unit in that
  # direction value is used when reporting position and direction
  DIRECTIONS = {
    north: {
      move: {
        delta_x: 0,
        delta_y: 1
      },
      left:  :west,
      right: :east,
      value: 'NORTH'
    },
    east: {
      move: {
        delta_x: 1,
        delta_y: 0
      },
      left:  :north,
      right: :south,
      value: 'EAST'
    },
    south: {
      move: {
        delta_x: 0,
        delta_y: -1
      },
      left:  :east,
      right: :west,
      value: 'SOUTH'
    },
    west: {
      move: {
        delta_x: -1,
        delta_y: 0
      },
      left:  :south,
      right: :north,
      value: 'WEST'
    }
  }.freeze

  MIN_X = 0
  MIN_Y = 0
  MAX_X = ((ENV['GRID_SIZE'] || 5).to_i - 1).freeze
  MAX_Y = ((ENV['GRID_SIZE'] || 5).to_i - 1).freeze

  # Represents robot moving on a grid
  class Robot
    def place(pos_x, pos_y, direction)
      return unless valid_position?(pos_x, pos_y)

      @pos_x = pos_x
      @pos_y = pos_y
      @direction = direction
      self
    end

    def move
      return unless placed?
      new_x = @pos_x + DIRECTIONS[@direction][:move][:delta_x]
      new_y = @pos_y + DIRECTIONS[@direction][:move][:delta_y]

      return unless valid_position?(new_x, new_y)

      @pos_x = new_x
      @pos_y = new_y
      self
    end

    # rotate 90 degrees anti-clockwise
    def left
      return unless placed?
      @direction = DIRECTIONS[@direction][:left]
      self
    end

    # rotate 90 degrees clockwise
    def right
      return unless placed?
      @direction = DIRECTIONS[@direction][:right]
      self
    end

    def report
      return unless placed?
      [@pos_x, @pos_y, DIRECTIONS[@direction][:value]]
    end

    private

    def placed?
      @pos_x && @pos_y && @direction
    end

    def valid_position?(pos_x, pos_y)
      pos_x.between?(MIN_X, MAX_X) && pos_y.between?(MIN_Y, MAX_Y)
    end
  end
end
