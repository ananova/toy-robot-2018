# frozen_string_literal: true

RSpec.describe ToyRobot do
  describe '.parse_command' do
    subject { ToyRobot.parse_command(command_string) }

    context 'when PLACE' do
      let(:command_string) { " \tPLACE  2 \t,  3 ,\tSOUTH\t" }

      it { is_expected.to eq [:place, 2, 3, :south] }
    end

    context 'when MOVE' do
      let(:command_string) { " \tMOVE  \t" }

      it { is_expected.to eq [:move] }
    end

    context 'when LEFT' do
      let(:command_string) { " \tLEFT  \t" }

      it { is_expected.to eq [:left] }
    end

    context 'when RIGHT' do
      let(:command_string) { " \tRIGHT  \t" }

      it { is_expected.to eq [:right] }
    end

    context 'when REPORT' do
      let(:command_string) { " \tREPORT  \t" }

      it { is_expected.to eq [:report] }
    end
  end

  describe '.run' do
    let(:input_stream) { StringIO.new("PLACE 1, 2, NORTH\nREPORT\nexit") }
    let(:output_stream) { $stdout }

    subject { described_class.run(input_stream, output_stream) }

    it 'can read from stdin' do
      expect { subject }.to output("Output: 1, 2, NORTH\n").to_stdout
    end

    it 'can write to stdout' do
      expect { subject }.to output("Output: 1, 2, NORTH\n").to_stdout
    end

    context 'when input is a file' do
      let(:input_stream) { File.open('./spec/fixtures/test_input.txt') }

      it 'reads input from file' do
        expected = <<~OUTPUT
          Output: 1, 4, NORTH
          Output: 1, 4, WEST
          Output: 0, 4, WEST
          Output: 4, 3, SOUTH
          Output: 4, 2, SOUTH
          Output: 4, 1, SOUTH
          Output: 4, 0, WEST
          Output: 2, 0, WEST
          Output: 2, 1, NORTH
        OUTPUT

        expect { subject }.to output(expected).to_stdout
      end
    end

    context 'when output is a file' do
      let(:output_stream) { File.open('test_output.txt', 'w') }

      before do
        subject
        output_stream.close
      end

      after { `rm test_output.txt` }

      it 'writes to the file specified' do
        written = File.read('test_output.txt')
        expect(written).to eq("Output: 1, 2, NORTH\n")
      end
    end
  end
end
