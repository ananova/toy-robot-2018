# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ToyRobot::Robot do
  def expect_robot(pos_x, pos_y, direction)
    expect(robot.instance_variable_get(:@pos_x)).to eq(pos_x)
    expect(robot.instance_variable_get(:@pos_y)).to eq(pos_y)
    expect(robot.instance_variable_get(:@direction)).to eq(direction)
  end

  let(:robot) { described_class.new }

  describe '#place' do
    context 'when the robot is not already on the board' do
      it 'does not get placed when coordinates are invalid' do
        robot.place(-2, 3, :south)
        expect_robot(nil, nil, nil)
      end

      it 'gets placed with valid coordinates' do
        robot.place(2, 3, :south)
        expect_robot(2, 3, :south)
      end
    end

    context 'when the robot is already on the board' do
      let(:robot) { described_class.new.place(1, 2, :west) }

      it 'does not get re-placed when coordinates are invalid' do
        robot.place(-2, 3, :south)
        expect_robot(1, 2, :west)
      end

      it 'gets re-placed with valid coordinates' do
        robot.place(4, 1, :north)
        expect_robot(4, 1, :north)
      end
    end
  end

  describe '#move' do
    before { robot.move }

    context 'when the robot is not on the board' do
      it 'does not move' do
        expect_robot(nil, nil, nil)
      end
    end

    context 'when the robot is on the board' do
      context 'when a move is invalid' do
        let(:robot) { described_class.new.place(4, 1, :east) }

        it 'does not move' do
          expect_robot(4, 1, :east)
        end
      end

      context 'when a move is valid' do
        let(:robot) { described_class.new.place(4, 1, :west) }

        it 'moves 1 unit in current direction' do
          expect_robot(3, 1, :west)
        end
      end
    end
  end

  describe '#left' do
    it 'does nothing when not placed' do
      expect(robot.left).to be_nil
    end

    it 'turns from north to west' do
      robot.place(1, 2, :north).left
      expect_robot(1, 2, :west)
    end

    it 'turns from east to north' do
      robot.place(1, 4, :east).left
      expect_robot(1, 4, :north)
    end

    it 'turns from south to east' do
      robot.place(3, 2, :south).left
      expect_robot(3, 2, :east)
    end

    it 'turns from west to south' do
      robot.place(4, 0, :west).left
      expect_robot(4, 0, :south)
    end
  end

  describe '#right' do
    it 'does nothing when not placed' do
      expect(robot.right).to be_nil
    end

    it 'turns from north to east' do
      robot.place(1, 2, :north).right
      expect_robot(1, 2, :east)
    end

    it 'turns from east to south' do
      robot.place(1, 4, :east).right
      expect_robot(1, 4, :south)
    end

    it 'turns from south to west' do
      robot.place(3, 2, :south).right
      expect_robot(3, 2, :west)
    end

    it 'turns from west to north' do
      robot.place(4, 0, :west).right
      expect_robot(4, 0, :north)
    end
  end

  describe '#report' do
    context 'when the robot is not on the board' do
      it 'returns nil' do
        expect(robot.report).to eq nil
      end
    end

    context 'when the robot is on the board' do
      let(:robot) { described_class.new.place(1, 2, :west) }

      it 'gives the position and direction as an array' do
        expect(robot.report).to eq [1, 2, 'WEST']
      end
    end
  end
end
