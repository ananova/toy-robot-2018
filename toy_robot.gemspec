# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'toy_robot/version'

Gem::Specification.new do |spec|
  spec.name          = 'toy-robot-ad-2018'
  spec.version       = ToyRobot::VERSION
  spec.authors       = ['Ana Djordjevic']
  spec.email         = ['me@ana.dj']

  spec.summary       = 'Toy robot simulator.'
  spec.description   = 'Simulator for a toy robot on a 5x5 grid.  The robot \
    can read from standard in or a file.  Refer to README for instructions.'
  spec.homepage      = 'https://github.com/ananova/toy-robot-ad-2018'
  spec.license       = 'MIT'

  # Files to be added to the gem when it is released.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    # Load the files in the RubyGem that have been added into git.
    `git ls-files -z`
      .split("\x0")
      .reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rspec-its'
  spec.add_development_dependency 'pry'
end
